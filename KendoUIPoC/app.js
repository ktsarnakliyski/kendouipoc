﻿var viewModel = kendo.observable({
    isVisible: true,
    onResize: function (e) {
    },
    onExpand: function (e) {
    },
    onCollapse: function (e) {
    }
});
kendo.bind($("#example"), viewModel);

var menu = kendo.observable({
    isVisible: true,
    onSelect: function (e) {
    }
});
kendo.bind($("#menu"), menu);

var tree = kendo.observable({
    isVisible: true,
    onSelect: function (e) {
    },
    files: kendo.observableHierarchy([
        {
            name: "Form", type: "folder", expanded: true, items: [
              {
                  name: "Page 1", type: "folder", expanded: true, items: [
                    { name: "textbox", type: "folder" },
                    { name: "radio", type: "folder" }
                  ]
              },
              {
                  name: "Page 2", type: "folder", expanded: true, items: [
                    { name: "radio", type: "folder" },
                    { name: "checkbox", type: "folder" }
                  ]
              }
            ]
        }
    ])
});
kendo.bind($("#tree"), tree);

function droptargetOnDragEnter(e) {
    $("#droptarget").addClass("painted");
}

function droptargetOnDragLeave(e) {
    $("#droptarget").removeClass("painted");
}

function droptargetOnDrop(e) {
    var draggableElement = e.draggable.currentTarget.clone().addClass("draggable-inside").removeClass("selected");
    $("#droptarget").append(draggableElement);
    $(".draggable-inside").kendoDraggable({
        container: $("#droptarget"),
        hint: function (e) {
            return $(e).addClass("selected");
        },
        dragend: draggableOnDragEnd
    });
}

function draggableOnDragEnd(e) {

}

$(document).ready(function () {
    var styles = [
                        { text: "Default", value: "styles/kendo.default.min.css" },
                        { text: "Material", value: "styles/kendo.material.min.css" },
                        { text: "Material Black", value: "styles/kendo.materialblack.min.css" },
                        { text: "Metro", value: "styles/kendo.metro.min.css" },
                        { text: "Metro Black", value: "styles/kendo.metroblack.min.css" },
                        { text: "Blue Opal", value: "styles/kendo.blueopal.min.css" },
                        { text: "Black", value: "styles/kendo.black.min.css" },
                        { text: "Silver", value: "styles/kendo.silver.min.css" },
                        { text: "Bootstrap", value: "styles/kendo.bootstrap.min.css" },
                        { text: "Fiori", value: "styles/kendo.fiori.min.css" },
                        { text: "Office 365", value: "styles/kendo.office365.min.css" },
                        { text: "Moonlight", value: "styles/kendo.moonlight.min.css" },
                        { text: "Flat", value: "styles/kendo.flat.min.css" }
    ];
    function onChange() {
        var value = $("#style").val();
        $("#kendoStyle").attr("href", value);
        var position = value.indexOf('.') + 1;
        var output = [value.slice(0, position), 'dataviz.', value.slice(position)].join('');
        $("#kendoStyleDataViz").attr("href", output);
    };
    // create DropDownList from input HTML element
    $("#style").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: styles,
        index: 0,
        change: onChange
    });

    $(".draggable").kendoDraggable({
        hint: function (e) {
            return $(e).clone().addClass("selected");
        },
        dragend: draggableOnDragEnd
    });

    $("#droptarget").kendoDropTarget({
        dragenter: droptargetOnDragEnter,
        dragleave: droptargetOnDragLeave,
        drop: droptargetOnDrop
    });
});

